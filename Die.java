import java.util.Random;
public class Die
{
    private int pips;
    private Random randomNums;

    public Die()
    {
        this.pips = 1;
        this.randomNums = new Random();
    }

    public int getPips()
    {
        return this.pips;
    }

    public int roll()
    {
        return this.pips = this.randomNums.nextInt(6)+1 ;
    }

    
    public String toString()
    {
        return "the dots : " + pips ;
    }

}

