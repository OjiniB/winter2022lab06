public class Board
{
    private Die die1;
    private Die die2;
    private boolean[] closedTiles;

    public Board()
    {
        this.die1 = new Die();
        this.die2 = new Die();
        this.closedTiles = new boolean[12];
    }


    public boolean playATurn()
    {
        int dieFirst= die1.roll();
        int dieSecond = die2.roll();
        System.out.println("= = = = = = = = = = = = = = = = = = = = =\n");
        System.out.println("After the shut: \n");
        System.out.println(">>> ^ First DIE: " + dieFirst + " , ^ Second DIE: " + dieSecond);
        int sum;
        sum = dieFirst + dieSecond;
        if (closedTiles[sum-1] == false)
        {
            closedTiles[sum-1] = true;
            System.out.println("Closing tile: with the tile number: " + sum);
            return false;
        }
        else
        {
			System.out.println("the sum is: " + sum);
            System.out.println("\nstarting the tile at this position is already shut");
            return true;
        }

    }
	
	public String toString()
    {
        String playing = " ";

        for (int i = 0 ; i < this.closedTiles.length ; i++)

        {
            if (closedTiles[i])
            {
                playing += "X" + " " ;

            }
            else
            {
                playing += (i + 1) + " " ;

            }


        }

        return 
		"\n-- Your Numbers are {" + playing + "} --\n\n" +
		"| first one " + this.die1 +
                ", Second one " + this.die2 + " |\n" 
           ;

    }


}
